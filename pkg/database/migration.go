package database

import (
	"database/sql"
	"log"

	"github.com/GuiaBolso/darwin"
	"github.com/davecgh/go-spew/spew"
)

var (
	mysqlMigrations = []darwin.Migration{
		{
			Version:     0.1,
			Description: "Creating table users",
			Script: `
CREATE TABLE users (
	id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	name varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
	role varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
	encoded_password varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
	salt text COLLATE utf8mb4_unicode_ci NOT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY name (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;`,
		},
		{
			Version:     0.2,
			Description: "Create first admin user",
			Script: `
INSERT INTO users (name, role, encoded_password, salt) VALUES 
('admin', 'admin', '07aaa952b02e68a3bb0d95ca99611a617caf23c68263bd44db6ec3f1d5b6ca21eba9d71aa2b009b72ea118dc4714373dc492', 'vUxEKMlhJHs749nF');`,
		},
	}
)

func startMysqlMigration(db *sql.DB, debug bool) error {
	driver := darwin.NewGenericDriver(db, darwin.MySQLDialect{})

	infoChain := make(chan darwin.MigrationInfo)

	go func(info chan darwin.MigrationInfo) {
		for i := range info {
			if debug {
				log.Printf("[Migration] %s", spew.Sdump(i))
			}
		}
	}(infoChain)

	d := darwin.New(driver, mysqlMigrations, infoChain)
	err := d.Migrate()

	close(infoChain)

	return err
}
