package database

import (
	"bytes"
	"strings"

	_ "github.com/go-sql-driver/mysql" // nolint
	"github.com/jmoiron/sqlx"
)

func NewMysql(mysqlDataSourceName string, debug bool) (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", mysqlDataSourceName)
	if err != nil {
		return nil, err
	}

	return NewDB(db, debug)
}

func NewDB(db *sqlx.DB, debug bool) (*sqlx.DB, error) {
	err := db.Ping()
	if err != nil {
		return nil, err
	}

	db.MapperFunc(toDBName)

	err = startMysqlMigration(db.DB, debug)
	if err != nil {
		return nil, err
	}

	return db, nil
}

// nolint
// toDBName func from GORM
func toDBName(name string) string {
	type strCase bool

	//noinspection GoUnusedConst
	const (
		lower strCase = false
		upper strCase = true
	)

	var (
		value                        = name
		buf                          = bytes.NewBufferString("")
		lastCase, currCase, nextCase strCase
	)

	for i, v := range value[:len(value)-1] {
		nextCase = strCase(value[i+1] >= 'A' && value[i+1] <= 'Z')
		if i > 0 {
			if currCase == upper {
				if lastCase == upper && nextCase == upper {
					buf.WriteRune(v)
				} else {
					if value[i-1] != '_' && value[i+1] != '_' {
						buf.WriteRune('_')
					}
					buf.WriteRune(v)
				}
			} else {
				buf.WriteRune(v)
				if i == len(value)-2 && nextCase == upper {
					buf.WriteRune('_')
				}
			}
		} else {
			currCase = upper
			buf.WriteRune(v)
		}
		lastCase = currCase
		currCase = nextCase
	}
	buf.WriteByte(value[len(value)-1])
	return strings.ToLower(buf.String())
}
