package random

import (
	"crypto/rand"
	"math/big"
)

// from https://github.com/gogits/gogs/blob/master/pkg/tool/tool.go
const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

// RandomString returns generated random string in given length of characters.
// It also returns possible error during generation.
func String(n int) (string, error) {
	buffer := make([]byte, n)
	max := big.NewInt(int64(len(alphanum)))

	for i := 0; i < n; i++ {
		index, err := Int(max)
		if err != nil {
			return "", err
		}

		buffer[i] = alphanum[index]
	}

	return string(buffer), nil
}

func Int(max *big.Int) (int, error) {
	r, err := rand.Int(rand.Reader, max)
	if err != nil {
		return 0, err
	}

	return int(r.Int64()), nil
}
