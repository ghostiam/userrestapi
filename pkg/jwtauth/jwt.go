package jwtauth

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	jwt.StandardClaims
	UserID int64
	Name   string
	Role   string
}

func generateUserJWT(userID int64, name, role string, expiresAt time.Time, key []byte) (string, error) {
	claims := Claims{
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: expiresAt.Unix(),
		},
		UserID: userID,
		Name:   name,
		Role:   role,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenSigned, err := token.SignedString(key)
	return tokenSigned, err
}

func getDataFromJWT(token string, key []byte) (*Claims, error) {
	var claims Claims

	u, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		return nil, err
	}

	if !u.Valid {
		return nil, jwt.ValidationError{}
	}

	if user, ok := u.Claims.(*Claims); ok {
		claims = *user
	}

	return &claims, nil
}
