package jwtauth

import (
	"net/http"
)

func Middleware(auth JWTAuth) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := auth.UserFromHTTPToken(r)

			ctx := r.Context()
			ctx = WithContext(ctx, auth)
			ctx = UserWithContext(ctx, user)
			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)
		})
	}
}
