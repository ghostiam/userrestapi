package jwtauth

import (
	"context"
)

type authContext string

var authContextKey authContext = "AuthKey"
var authUserContextKey authContext = "AuthUserKey"

func WithContext(ctx context.Context, auth JWTAuth) context.Context {
	return context.WithValue(ctx, authContextKey, auth)
}

func FromContext(ctx context.Context) (JWTAuth, bool) {
	auth, ok := ctx.Value(authContextKey).(JWTAuth)
	return auth, ok
}

func UserWithContext(ctx context.Context, user User) context.Context {
	return context.WithValue(ctx, authUserContextKey, user)
}

func UserFromContext(ctx context.Context) (User, bool) {
	user, ok := ctx.Value(authUserContextKey).(User)
	return user, ok
}
