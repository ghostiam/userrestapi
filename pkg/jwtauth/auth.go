package jwtauth

import (
	"net/http"
	"strings"
	"time"

	"bitbucket.org/ghostiam/userRESTAPI/models"
)

type User struct {
	IsAuth bool
	ID     int64
	Name   string
	Role   models.Role
}

type auth struct {
	key            []byte
	tokenExpiresAt time.Duration
}

type JWTAuth interface {
	GenerateToken(id int64, name string, role string) (string, error)
	UserFromToken(token string) User
	UserFromHTTPToken(r *http.Request) User
}

func NewAuth(key []byte, tokenExpiresAt time.Duration) JWTAuth {
	return auth{
		key:            key,
		tokenExpiresAt: tokenExpiresAt,
	}
}

func (a auth) GenerateToken(id int64, name string, role string) (string, error) {
	return generateUserJWT(id, name, role, time.Now().Add(a.tokenExpiresAt), a.key)
}

func (a auth) UserFromToken(token string) User {
	claims, err := getDataFromJWT(token, a.key)
	if err != nil {
		return User{}
	}

	return User{
		IsAuth: true,
		ID:     claims.UserID,
		Name:   claims.Name,
		Role:   models.Role(claims.Role),
	}
}

func (a auth) UserFromHTTPToken(r *http.Request) User {
	const bearerScheme = "Bearer "
	authHeader := r.Header.Get("Authorization")
	if strings.HasPrefix(authHeader, bearerScheme) {
		token := authHeader[len(bearerScheme):]
		return a.UserFromToken(token)
	}

	return User{}
}
