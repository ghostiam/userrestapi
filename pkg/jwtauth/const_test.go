package jwtauth

import (
	"time"
)

// for test
var key = []byte("TestJWTKey")

func getExpiredToken() string {
	t := time.Now().Add(-24 * time.Hour)
	token, err := generateToken(t)
	if err != nil {
		panic(err)
	}
	return token
}

func getValidToken() string {
	t := time.Now().Add(24 * time.Hour)
	token, err := generateToken(t)
	if err != nil {
		panic(err)
	}
	return token
}

func generateToken(expiresAt time.Time) (string, error) {
	return generateUserJWT(42, "TestName", "TestRole", expiresAt, key)
}
