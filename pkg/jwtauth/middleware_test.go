package jwtauth

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMiddleware(t *testing.T) {
	type args struct {
		token string
	}
	type want struct {
		isAuth bool
		userID int64
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "Valid token",
			args: args{
				token: getValidToken(),
			},
			want: want{
				isAuth: true,
				userID: 42,
			},
		},
		{
			name: "Expired valid token",
			args: args{
				token: getExpiredToken(),
			},
			want: want{
				isAuth: false,
				userID: 0,
			},
		},
		{
			name: "Empty token",
			args: args{
				token: "",
			},
			want: want{
				isAuth: false,
				userID: 0,
			},
		},
		{
			name: "Not valid token",
			args: args{
				token: "NotValidToken",
			},
			want: want{
				isAuth: false,
				userID: 0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/testJWT", nil)
			require.Nil(t, err)
			if tt.args.token != "" {
				req.Header.Set("Authorization", "Bearer "+tt.args.token)
			}

			rr := httptest.NewRecorder()

			a := NewAuth(key, 0)
			middleware := Middleware(a)

			validateHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				ctx := r.Context()

				_, ok := FromContext(ctx)
				require.Equal(t, true, ok)

				user, ok := UserFromContext(ctx)
				require.Equal(t, true, ok)
				require.Equal(t, tt.want.isAuth, user.IsAuth)
				require.Equal(t, tt.want.userID, user.ID)

			})

			handler := middleware(validateHandler)
			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != http.StatusOK {
				t.Errorf("handler returned wrong status code: got %v want %v (%v)",
					status, http.StatusOK, rr.Body)
			}
		})
	}
}
