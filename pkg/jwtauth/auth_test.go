package jwtauth

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestGenerateTokenAndUserFromToken(t *testing.T) {
	// vars
	type fields struct {
		key            []byte
		tokenExpiresAt time.Duration
	}
	type args struct {
		userID int64
		name   string
		role   string
	}
	type want struct {
		userID int64
		isAuth bool
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   want
	}{
		{
			name: "first",
			fields: fields{
				key:            nil,
				tokenExpiresAt: 1 * time.Hour,
			},
			args: args{
				userID: 42,
				name:   "TestUserName",
				role:   "TestRole",
			},
			want: want{
				userID: 42,
				isAuth: true,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			auth := NewAuth(tt.fields.key, tt.fields.tokenExpiresAt)

			token, err := auth.GenerateToken(tt.args.userID, tt.args.name, tt.args.role)
			require.Nil(t, err)
			require.NotEmpty(t, token)

			userData := auth.UserFromToken(token)
			require.Equal(t, tt.want.userID, userData.ID)
			require.Equal(t, tt.want.isAuth, userData.IsAuth)
		})
	}
}

func TestFromHttp(t *testing.T) {
	type args struct {
		token string
	}
	type want struct {
		isAuth bool
		userID int64
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "Valid token",
			args: args{
				token: getValidToken(),
			},
			want: want{
				isAuth: true,
				userID: 42,
			},
		},
		{
			name: "Expired valid token",
			args: args{
				token: getExpiredToken(),
			},
			want: want{
				isAuth: false,
				userID: 0,
			},
		},
		{
			name: "Empty token",
			args: args{
				token: "",
			},
			want: want{
				isAuth: false,
				userID: 0,
			},
		},
		{
			name: "Not valid token",
			args: args{
				token: "NotValidToken",
			},
			want: want{
				isAuth: false,
				userID: 0,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/testJWT", nil)
			require.Nil(t, err)
			if tt.args.token != "" {
				req.Header.Set("Authorization", "Bearer "+tt.args.token)
			}

			rr := httptest.NewRecorder()

			auth := NewAuth(key, 1*time.Hour)

			validateHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				user := auth.UserFromHTTPToken(r)

				require.Equal(t, tt.want.isAuth, user.IsAuth)
				require.Equal(t, tt.want.userID, user.ID)
			})

			validateHandler.ServeHTTP(rr, req)

			if status := rr.Code; status != http.StatusOK {
				t.Errorf("handler returned wrong status code: got %v want %v (%v)",
					status, http.StatusOK, rr.Body)
			}
		})
	}
}
