package models

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestUser_SetPassword(t *testing.T) {
	type args struct {
		password string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Test",
			args: args{
				password: "TestPassword",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{}

			// empty before test
			require.Empty(t, u.Salt)
			require.Empty(t, u.EncodedPassword)

			err := u.SetPassword(tt.args.password)
			require.Nil(t, err)

			// fill after test
			require.NotEmpty(t, u.Salt)
			require.NotEmpty(t, u.EncodedPassword)

			storeOldSalt, storeOldEncPass := u.Salt, u.EncodedPassword

			// set password again
			err = u.SetPassword(tt.args.password)
			require.Nil(t, err)

			// fill after test
			require.NotEmpty(t, u.Salt)
			// test regenerate salt
			require.NotEqual(t, storeOldSalt, u.Salt)
			require.NotEmpty(t, u.EncodedPassword)
			require.NotEqual(t, storeOldEncPass, u.EncodedPassword)
		})
	}
}

func TestUser_ValidatePassword(t *testing.T) {
	type fields struct {
		EncodedPassword string
		Salt            string
	}
	type args struct {
		password string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Test equal password",
			fields: fields{
				EncodedPassword: "ceac3597e222e03f94abd2f1b61e9d4387a8b8af062f4ef8d9a232ab42dc95c5b3753dab88aeaf59046b51e81c4aa6aee608",
				Salt:            "MySuperDuperSalt", // len 16
			},
			args: args{
				password: "TestPassword",
			},
			want: true,
		},
		{
			name: "Test equal other password",
			fields: fields{
				EncodedPassword: "82707cdaf6b34b87d747792cb83faabf2b897d6597050e50de5777b9a2567179a04694bb937bdda9ed2dabeb8e9f60b2b516",
				Salt:            "MySuperOtherSalt", // len 16
			},
			args: args{
				password: "TestPassword",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				EncodedPassword: tt.fields.EncodedPassword,
				Salt:            tt.fields.Salt,
			}

			got := u.ValidatePassword(tt.args.password)
			require.Equal(t, tt.want, got)
		})
	}
}
