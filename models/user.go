package models

import (
	"crypto/sha256"
	"fmt"

	"bitbucket.org/ghostiam/userRESTAPI/pkg/random"
	"github.com/pkg/errors"
	"golang.org/x/crypto/pbkdf2"
)

type User struct {
	ID   int64
	Name string
	Role Role

	EncodedPassword string
	Salt            string
}

// TODO: use enum generator
// Enum roles
type Role string

const (
	RoleUnknown Role = ""
	RoleUser    Role = "user"
	RoleAdmin   Role = "admin"
)

func (r Role) IsValid() bool {
	return r == RoleAdmin || r == RoleUser
}

func (r Role) String() string {
	return string(r)
}

//
func (u *User) SetPassword(password string) error {
	salt, err := generateSalt()
	if err != nil {
		return errors.Wrap(err, "failed generate salt")
	}

	u.Salt = salt
	u.EncodedPassword = encodePassword(password, u.Salt)
	return nil
}

func (u *User) ValidatePassword(password string) bool {
	return u.EncodedPassword == encodePassword(password, u.Salt)
}

func generateSalt() (string, error) {
	return random.String(16)

}

func encodePassword(password, salt string) string {
	newPassword := pbkdf2.Key([]byte(password), []byte(salt), 10000, 50, sha256.New)
	return fmt.Sprintf("%x", newPassword)
}
