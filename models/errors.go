package models

// TODO: replace to struct with code and message
type Error string

func (e Error) Error() string {
	return string(e)
}

const (
	// Common
	ErrNotFound = Error("Not found")
	ErrServer   = Error("Internal Server Error")

	// User
	ErrUserNameEmpty     = Error("Name is empty")
	ErrUserNameConflict  = Error("Name conflict")
	ErrUserPasswordEmpty = Error("Password is empty")
	ErrUserRoleInvalid   = Error("Role invalid")
	ErrUserRoleRejected  = Error("Role rejected")
	ErrUserLoginInvalid  = Error("Invalid login or password")
)
