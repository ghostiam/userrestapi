package models

type APIErrors struct {
	Errors []APIError
}

type APIError struct {
	Code    int64
	Message string
}
