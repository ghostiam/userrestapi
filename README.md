# User REST API

## Тестовое задание:
1. Создать API для управления пользователями в таблице users.
2. API должно поддерживать операции:
    - получение списка пользователей
    - добавления нового пользователя
    - удаление пользователя
3. API должно следовать REST.
4. Пользователь могут быть двух ролей: admin или user.
5. Admin может выполнять все операции.
6. User может:
    - получать список пользователей с ролью user
    - добавлять нового пользователя с ролью user.
7. Пользователи должны быть авторизованы любым способом для выполнения операций.

## Info
При первом запуске, создаётся пользователь `admin` с паролем `admin` и ролью `admin`

## Api spec
Спецификация API доступна в файле [openapi.yml](./static/openapi.yml) и интерфейс Swagger UI по адресу запущенного сервиса `http://localhost:3000/`

## docker
```bash
docker-compose up -d
# or 
docker build -t user-api .
docker run -it --rm user-api
```

## building
```bash
make deps  # install dependence
make test  # run tests
make build # building create files in ./dist folder
```