GOCMD=go
GOBUILD=$(GOCMD) build
GOTEST=$(GOCMD) test
GOLIST=$(GOCMD) list
GOGET=$(GOCMD) get
GOMETALINTER=gometalinter.v2

CMD_DIR=./cmd/server
BINARY_DIR=dist
BINARY_NAME=server
BINARY_UNIX=$(BINARY_NAME)_unix

COMMIT := `git rev-parse HEAD`

BINARY_PATH=$(BINARY_DIR)/$(BINARY_NAME)
BINARY_PATH_UNIX=$(BINARY_DIR)/$(BINARY_UNIX)
LDFLAGS=-ldflags "-X=main.Commit=$(COMMIT)"

build: generate
	CGO_ENABLED=0 $(GOBUILD) $(LDFLAGS) -o $(BINARY_PATH) -v $(CMD_DIR)
	@cp ./config.example.json $(BINARY_DIR)/config.json
	@cp -R ./static $(BINARY_DIR)/static
build-linux: generate
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) $(LDFLAGS) -o $(BINARY_PATH_UNIX) -v $(CMD_DIR)
	@cp ./config.example.json $(BINARY_DIR)/config.json
	@cp -R ./static $(BINARY_DIR)/static
run: build
	./$(BINARY_PATH)

generate: clean-generate
	@$(GOCMD) generate ./...

test: generate
	@$(GOTEST) ./...

cover: MOCK_NAME=mock_
cover: generate
	@echo 'mode: atomic' > coverage.coverprofile
	@$(GOLIST) -f '{{if gt (len .TestGoFiles) 0}}"go test -covermode atomic -coverprofile {{.Name}}.coverprofile.tmp -coverpkg ./... {{.ImportPath}} && tail -n +2 {{.Name}}.coverprofile.tmp | grep -v "$(MOCK_NAME)" >> coverage.coverprofile"{{end}}' ./... | xargs -I {} bash -c {}
	@rm *.coverprofile.tmp
	@$(GOCMD) tool cover -html=coverage.coverprofile
	@rm coverage.coverprofile

clean: clean-build clean-generate
clean-build:
	@rm -f $(BINARY_PATH)
	@rm -f $(BINARY_PATH_UNIX)
clean-generate:
	@find . -name "mock_*.go" -type f -delete

metalinter-fast: ARGS=--fast
metalinter-fast: .metalinter

metalinter: ARGS=--exclude=duplicate.of.*_test.go --enable=dupl --enable=gosimple
metalinter: .metalinter

.metalinter:
	@$(GOMETALINTER) --exclude=have.comment --exclude=comment.on --exclude=mock_ -j4 --vendor --enable-gc --enable=nakedret --sort=path $(ARGS) -t ./...

deps:
	$(GOGET) -u github.com/vektah/gorunpkg
	$(GOGET) -u gopkg.in/alecthomas/gometalinter.v2
	$(GOMETALINTER) -i

precommit: generate metalinter-fast test