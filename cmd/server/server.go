package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	authDelivery "bitbucket.org/ghostiam/userRESTAPI/internal/auth/delivery/http"
	authUsecase "bitbucket.org/ghostiam/userRESTAPI/internal/auth/usecase"
	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	userDelivery "bitbucket.org/ghostiam/userRESTAPI/internal/user/delivery/http"
	userRepo "bitbucket.org/ghostiam/userRESTAPI/internal/user/repository"
	userUsecase "bitbucket.org/ghostiam/userRESTAPI/internal/user/usecase"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"bitbucket.org/ghostiam/userRESTAPI/pkg/database"
	"bitbucket.org/ghostiam/userRESTAPI/pkg/jwtauth"
	"bitbucket.org/ghostiam/userRESTAPI/pkg/rest"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

var Commit = "-dev-"

func main() {
	fmt.Printf("Version: %s\n", Commit)

	// Config
	cfg := getConfigure("config.json")
	jwtSecret := []byte(cfg.JWTSecret)
	jwtTokenExpiresAt := time.Duration(cfg.JWTTokenExpiresMinutes) * time.Minute
	//

	// setup database
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?timeout=30s&parseTime=true", cfg.DB.Username, cfg.DB.Password, cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)
	db, err := database.NewMysql(dsn, true)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	uRepo := userRepo.NewUserRepository(db)

	// setup auth middleware
	auth := jwtauth.NewAuth(jwtSecret, jwtTokenExpiresAt)
	authMiddleware := jwtauth.Middleware(auth)

	// main route
	mainRouter := mux.NewRouter()

	// api version router
	v0Router := mainRouter.PathPrefix("/v0").Subrouter()

	// auth router
	authRoute := v0Router.PathPrefix("/auth").Subrouter()
	authDelivery.NewAuthHTTPHandler(authRoute, authUsecase.New(uRepo, auth))

	// user router
	userRoute := v0Router.PathPrefix("/users").Subrouter()
	userRoute.Use(authMiddleware, checkAuthMiddleware) // use Auth and checker middleware
	userDelivery.NewUserHTTPHandler(userRoute, func(privileges models.Role) user.Usecase {
		return userUsecase.New(privileges, uRepo)
	})

	// static files route
	mainRouter.PathPrefix("/").Handler(http.FileServer(http.Dir("./static")))

	// start server
	log.Printf("Server started %s\n", cfg.ServerAddr)
	log.Fatal(http.ListenAndServe(cfg.ServerAddr, handlers.CORS(
		handlers.AllowedHeaders([]string{"Accept", "Accept-Language", "Content-Language", "Origin", "Authorization"}),
		handlers.AllowedMethods([]string{http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete}),
	)(mainRouter)))
}

type Config struct {
	ServerAddr             string
	JWTSecret              string
	JWTTokenExpiresMinutes int

	DB struct {
		Host     string
		Port     string
		Username string
		Password string
		Name     string
	}
}

func getConfigure(filename string) Config {
	f, err := os.Open(filename) // nolint
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	var cfg Config
	err = json.NewDecoder(f).Decode(&cfg)
	if err != nil {
		log.Fatal(err)
	}

	return cfg
}

func checkAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		u, ok := jwtauth.UserFromContext(ctx)
		if !ok || !u.IsAuth {
			apiErr := models.APIError{
				Code:    0, // TODO
				Message: "Unauthorized",
			}

			rest.WriteJSON(w, http.StatusUnauthorized, apiErr)
			return
		}

		next.ServeHTTP(w, r)
	})
}
