package auth

import (
	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
)

//go:generate gorunpkg github.com/vektra/mockery/cmd/mockery -inpkg -name Usecase
type Usecase interface {
	Login(name, password string) (*UserWithToken, error)
}

type UserWithToken struct {
	Token string
	User  user.User
}
