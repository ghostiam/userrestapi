package usecase

import (
	"testing"

	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_authUsecase_Login(t *testing.T) {
	mockUser := &models.User{
		ID:   42,
		Name: "TestName",
		Role: "admin",
	}
	mockUser.SetPassword("TestPass") // nolint

	type args struct {
		name     string
		password string
	}
	type mocks struct {
		getUser *models.User
		getErr  error
		token   string
	}
	type want struct {
		user *user.User
		err  error
	}
	tests := []struct {
		name string
		args args
		mock mocks
		want want
	}{
		{
			name: "Not exist",
			args: args{
				name:     "TestName",
				password: "TestPass",
			},
			mock: mocks{
				getUser: nil,
			},
			want: want{
				err: models.ErrUserLoginInvalid,
			},
		},
		{
			name: "Exist",
			args: args{
				name:     "TestName",
				password: "TestPass",
			},
			mock: mocks{
				getUser: mockUser,
				getErr:  nil,
				token:   "TestToken",
			},
			want: want{
				user: &user.User{
					ID:   42,
					Name: "TestName",
					Role: models.RoleAdmin,
				},
				err: nil,
			},
		},
		{
			name: "NotValidPass",
			args: args{
				name:     "TestName",
				password: "NotValid",
			},
			mock: mocks{
				getUser: mockUser,
				getErr:  nil,
			},
			want: want{
				user: nil,
				err:  models.ErrUserLoginInvalid,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup
			repo := &user.MockRepository{}
			defer repo.AssertExpectations(t)

			repo.On("GetByName", tt.args.name).Return(tt.mock.getUser, tt.mock.getErr).Once()

			a := &MockAuth{}
			defer a.AssertExpectations(t)

			if tt.mock.token != "" {
				a.On("GenerateToken",
					tt.mock.getUser.ID,
					tt.mock.getUser.Name,
					tt.mock.getUser.Role.String(),
				).Return(tt.mock.token, nil).Once()
			}
			// test
			authUsecase := New(repo, a)
			uwt, err := authUsecase.Login(tt.args.name, tt.args.password)

			// check
			assert.Equal(t, tt.want.err, err)
			if tt.want.user != nil {
				require.NotNil(t, uwt)
				assert.Equal(t, tt.mock.token, uwt.Token)
				assert.Equal(t, *tt.want.user, uwt.User)
			}
		})
	}
}
