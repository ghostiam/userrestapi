package usecase

import (
	"bitbucket.org/ghostiam/userRESTAPI/internal/auth"
	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/pkg/errors"
)

//go:generate gorunpkg github.com/vektra/mockery/cmd/mockery -inpkg -name Auth
type Auth interface {
	GenerateToken(id int64, name string, role string) (string, error)
}

type authUsecase struct {
	userRepo user.Repository
	auth     Auth
}

func New(ur user.Repository, auth Auth) auth.Usecase {
	return &authUsecase{
		userRepo: ur,
		auth:     auth,
	}
}

func (a *authUsecase) Login(name, password string) (*auth.UserWithToken, error) {
	userModel, err := a.userRepo.GetByName(name)
	if err != nil {
		return nil, errors.Wrap(err, "failed get user by name")
	}

	if userModel == nil {
		return nil, models.ErrUserLoginInvalid
	}

	isValidPass := userModel.ValidatePassword(password)
	if !isValidPass {
		return nil, models.ErrUserLoginInvalid
	}

	uUser := convertUser(userModel)
	token, err := a.auth.GenerateToken(uUser.ID, uUser.Name, uUser.Role.String())
	if err != nil {
		return nil, errors.Wrap(err, "failed generate token")
	}

	return &auth.UserWithToken{
		Token: token,
		User:  *uUser,
	}, nil
}

func convertUser(um *models.User) *user.User {
	if um == nil {
		return nil
	}

	return &user.User{
		ID:   um.ID,
		Name: um.Name,
		Role: um.Role,
	}
}
