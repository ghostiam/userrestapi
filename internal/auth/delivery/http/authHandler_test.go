package http

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"bitbucket.org/ghostiam/userRESTAPI/internal/auth"
	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
)

func Test_authHandler_Login(t *testing.T) {
	type args struct {
		name     string
		password string
	}
	type mocks struct {
		user *auth.UserWithToken
		err  error
	}
	type want struct {
		statusCode int
		body       string
	}
	tests := []struct {
		name string
		args args
		mock mocks
		want want
	}{
		{
			name: "Login",
			args: args{
				name:     "TestName",
				password: "TestPass",
			},
			mock: mocks{
				user: &auth.UserWithToken{
					Token: "TestToken",
					User: user.User{
						ID:   42,
						Name: "TestName",
						Role: models.RoleAdmin,
					},
				},
				err: nil,
			},
			want: want{
				statusCode: http.StatusOK,
				body:       `{"Token":"TestToken", "User":{"ID":42, "Name":"TestName", "Role":"admin"}}`,
			},
		},
		{
			name: "Login error",
			args: args{
				name:     "TestName",
				password: "TestPass",
			},
			mock: mocks{
				user: nil,
				err:  models.ErrUserLoginInvalid,
			},
			want: want{
				statusCode: http.StatusUnauthorized,
				body:       `{"Code":0, "Message":"Invalid login or password"}`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup mock
			authUsecase := &auth.MockUsecase{}
			defer authUsecase.AssertExpectations(t)

			authUsecase.On("Login", tt.args.name, tt.args.password).Return(tt.mock.user, tt.mock.err).Once()

			// setup http test
			form := url.Values{}
			form.Set("name", tt.args.name)
			form.Set("password", tt.args.password)
			reqBody := strings.NewReader(form.Encode())

			urlAddr := "/auth/login"
			req, err := http.NewRequest(http.MethodPost, urlAddr, reqBody)
			require.NoError(t, err)
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			wRec := httptest.NewRecorder()

			// test
			m := mux.NewRouter().PathPrefix("/auth").Subrouter()
			NewAuthHTTPHandler(m, authUsecase)
			m.ServeHTTP(wRec, req)

			body := wRec.Body.String()
			// check
			require.Equal(t, tt.want.statusCode, wRec.Code, body)
			require.NoError(t, err, "failed read body")
			require.JSONEq(t, tt.want.body, body)
		})
	}
}
