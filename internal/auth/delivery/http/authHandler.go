package http

import (
	"net/http"

	"bitbucket.org/ghostiam/userRESTAPI/internal/auth"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"bitbucket.org/ghostiam/userRESTAPI/pkg/rest"
	"github.com/gorilla/mux"
)

func NewAuthHTTPHandler(router *mux.Router, au auth.Usecase) {
	h := &authHandler{
		au: au,
	}

	router.HandleFunc("/login", h.Login).Methods(http.MethodPost)
}

type authHandler struct {
	au auth.Usecase
}

func (h *authHandler) Login(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		writeError(w, err)
		return
	}

	v := r.PostForm
	name := v.Get("name")
	pass := v.Get("password")

	uwt, err := h.au.Login(name, pass)
	if err != nil {
		writeError(w, err)
		return
	}

	rest.WriteJSON(w, http.StatusOK, uwt)
}

// region Helpers
func writeError(w http.ResponseWriter, err error) {
	code := getStatusCode(err)

	apiErr := models.APIError{
		Code:    0, // TODO
		Message: err.Error(),
	}

	rest.WriteJSON(w, code, apiErr)
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	switch err {
	case models.ErrUserLoginInvalid:
		return http.StatusUnauthorized
	default:
		return http.StatusInternalServerError
	}
}

// endregion
