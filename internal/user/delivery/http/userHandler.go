package http

import (
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"bitbucket.org/ghostiam/userRESTAPI/pkg/jwtauth"
	"bitbucket.org/ghostiam/userRESTAPI/pkg/rest"
	"github.com/gorilla/mux"
)

func NewUserHTTPHandler(router *mux.Router, uFunc UsecaseFunc) {
	h := &userHandler{
		uFunc: uFunc,
	}

	router.HandleFunc("", h.Fetch).Methods(http.MethodGet)
	router.HandleFunc("", h.Store).Methods(http.MethodPost)

	router.HandleFunc("/{id:[0-9]+}", h.GetByID).Methods(http.MethodGet)
	router.HandleFunc("/{id:[0-9]+}", h.DeleteByID).Methods(http.MethodDelete)
}

type UsecaseFunc func(privileges models.Role) user.Usecase
type userHandler struct {
	uFunc UsecaseFunc
}

func (h *userHandler) Fetch(w http.ResponseWriter, r *http.Request) {
	p := currentPrivilege(r)
	u, err := h.uFunc(p).GetUsers()
	if err != nil {
		writeError(w, err)
		return
	}

	rest.WriteJSON(w, http.StatusOK, u)
}

func (h *userHandler) Store(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		writeError(w, err)
		return
	}

	name := r.PostForm.Get("name")
	password := r.PostForm.Get("password")
	role := r.PostForm.Get("role")

	p := currentPrivilege(r)
	u, errs := h.uFunc(p).StoreUser(name, password, models.Role(role))
	if len(errs) > 0 {
		// Write errors
		code := getStatusCode(errs[0])

		var apiErrs []models.APIError
		for _, e := range errs {
			apiErrs = append(apiErrs, models.APIError{
				Code:    0, // TODO
				Message: e.Error(),
			})
		}

		rest.WriteJSON(w, code, models.APIErrors{Errors: apiErrs})
		return
	}

	// TODO: logging this error
	if u == nil {
		writeError(w, errors.New("user is nil"))
	}

	rest.WriteJSON(w, http.StatusCreated, u)
}

func (h *userHandler) GetByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.ParseInt(idString, 10, 0)
	if err != nil {
		writeError(w, models.ErrNotFound)
		return
	}

	p := currentPrivilege(r)
	u, err := h.uFunc(p).GetUser(id)
	if err != nil {
		writeError(w, err)
		return
	}

	rest.WriteJSON(w, http.StatusOK, u)
}

func (h *userHandler) DeleteByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.ParseInt(idString, 10, 0)
	if err != nil {
		writeError(w, models.ErrNotFound)
		return
	}

	p := currentPrivilege(r)
	err = h.uFunc(p).DeleteUser(id)
	if err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func currentPrivilege(r *http.Request) models.Role {
	u, ok := jwtauth.UserFromContext(r.Context())
	if !ok || !u.IsAuth {
		return models.RoleUnknown
	}

	return u.Role
}

func writeError(w http.ResponseWriter, err error) {
	code := getStatusCode(err)

	apiErr := models.APIError{
		Code:    0, // TODO
		Message: err.Error(),
	}

	rest.WriteJSON(w, code, apiErr)
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	switch err {
	case models.ErrNotFound:
		return http.StatusNotFound
	case models.ErrUserNameConflict:
		return http.StatusConflict
	case models.ErrUserNameEmpty,
		models.ErrUserPasswordEmpty,
		models.ErrUserRoleInvalid:
		return http.StatusBadRequest
	case models.ErrUserRoleRejected:
		return http.StatusForbidden
	case models.ErrServer:
		return http.StatusInternalServerError
	default:
		return http.StatusInternalServerError
	}
}
