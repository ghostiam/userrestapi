package http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
)

func Test_userHandler_Fetch(t *testing.T) {
	type mocks struct {
		user []user.User
		err  error
	}
	type want struct {
		statusCode int
		body       string
	}
	tests := []struct {
		name string
		mock mocks
		want want
	}{
		{
			name: "Fetch",
			mock: mocks{
				user: []user.User{
					{ID: 123, Name: "Test123", Role: models.RoleAdmin},
					{ID: 456, Name: "Test456", Role: models.RoleUser},
				},
				err: nil,
			},
			want: want{
				statusCode: http.StatusOK,
				body:       `[{"ID": 123, "Name": "Test123", "Role": "admin"}, {"ID": 456, "Name": "Test456", "Role": "user"}]`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup mock
			uUsecase := &user.MockUsecase{}
			defer uUsecase.AssertExpectations(t)

			uUsecase.On("GetUsers").Return(tt.mock.user, tt.mock.err).Once()

			// setup http test
			urlAddr := "/users"
			req, err := http.NewRequest(http.MethodGet, urlAddr, nil)
			require.NoError(t, err)
			wRec := httptest.NewRecorder()

			// test
			m := mux.NewRouter().PathPrefix("/users").Subrouter()
			NewUserHTTPHandler(m, func(p models.Role) user.Usecase { return uUsecase })
			m.ServeHTTP(wRec, req)

			body := wRec.Body.String()
			// check
			require.Equal(t, tt.want.statusCode, wRec.Code, body)
			require.NoError(t, err, "failed read body")
			require.JSONEq(t, tt.want.body, body)
		})
	}
}

func Test_userHandler_Store(t *testing.T) {
	type args struct {
		name     string
		password string
		role     models.Role
	}
	type mocks struct {
		user *user.User
		errs []error
	}
	type want struct {
		statusCode int
		body       string
	}
	tests := []struct {
		name string
		args args
		mock mocks
		want want
	}{
		{
			name: "Conflict",
			mock: mocks{
				user: nil,
				errs: []error{models.ErrUserNameConflict},
			},
			want: want{
				statusCode: http.StatusConflict,
				body:       `{"Errors": [{"Code": 0, "Message": "Name conflict"}]}`,
			},
		},
		{
			name: "Bad Request",
			mock: mocks{
				user: nil,
				errs: []error{models.ErrUserNameEmpty},
			},
			want: want{
				statusCode: http.StatusBadRequest,
				body:       `{"Errors": [{"Code": 0, "Message": "Name is empty"}]}`,
			},
		},
		{
			name: "Many Bad Request",
			mock: mocks{
				user: nil,
				errs: []error{models.ErrUserNameEmpty, models.ErrUserPasswordEmpty, models.ErrUserRoleInvalid},
			},
			want: want{
				statusCode: http.StatusBadRequest,
				body:       `{"Errors": [{"Code": 0, "Message": "Name is empty"}, {"Code": 0, "Message": "Password is empty"}, {"Code": 0, "Message": "Role invalid"}]}`,
			},
		},
		{
			name: "Forbidden Role",
			mock: mocks{
				user: nil,
				errs: []error{models.ErrUserRoleRejected},
			},
			want: want{
				statusCode: http.StatusForbidden,
				body:       `{"Errors": [{"Code": 0, "Message": "Role rejected"}]}`,
			},
		},
		{
			name: "Store",
			args: args{
				name:     "TestName",
				password: "TestPassword",
				role:     models.RoleAdmin,
			},
			mock: mocks{
				user: &user.User{
					ID:   42,
					Name: "TestName",
					Role: models.RoleAdmin,
				},
				errs: nil,
			},
			want: want{
				statusCode: http.StatusCreated,
				body:       `{"ID":42,"Name":"TestName","Role":"admin"}`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup mock
			uUsecase := &user.MockUsecase{}
			defer uUsecase.AssertExpectations(t)

			uUsecase.On("StoreUser", tt.args.name, tt.args.password, tt.args.role).Return(tt.mock.user, tt.mock.errs).Once()

			// setup http test
			form := url.Values{}
			form.Set("name", tt.args.name)
			form.Set("password", tt.args.password)
			form.Set("role", tt.args.role.String())
			reqBody := strings.NewReader(form.Encode())

			urlAddr := "/users"
			req, err := http.NewRequest(http.MethodPost, urlAddr, reqBody)
			require.NoError(t, err)
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			wRec := httptest.NewRecorder()

			// test
			m := mux.NewRouter().PathPrefix("/users").Subrouter()
			NewUserHTTPHandler(m, func(p models.Role) user.Usecase { return uUsecase })
			m.ServeHTTP(wRec, req)

			body := wRec.Body.String()
			// check
			require.Equal(t, tt.want.statusCode, wRec.Code, body)
			require.NoError(t, err, "failed read body")
			require.JSONEq(t, tt.want.body, body)
		})
	}
}

func Test_userHandler_GetByID(t *testing.T) {
	type args struct {
		id int64
	}
	type mocks struct {
		user *user.User
		err  error
	}
	type want struct {
		statusCode int
		body       string
	}
	tests := []struct {
		name string
		args args
		mock mocks
		want want
	}{
		{
			name: "Not exist",
			args: args{
				id: 42,
			},
			mock: mocks{
				user: nil,
				err:  models.ErrNotFound,
			},
			want: want{
				statusCode: http.StatusNotFound,
				body:       `{"Code": 0, "Message": "Not found"}`,
			},
		},
		{
			name: "Exist",
			args: args{
				id: 42,
			},
			mock: mocks{
				user: &user.User{
					ID:   42,
					Name: "TestUser",
					Role: models.RoleAdmin,
				},
				err: nil,
			},
			want: want{
				statusCode: http.StatusOK,
				body:       `{"ID":42, "Name":"TestUser", "Role": "admin"}`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup mock
			uUsecase := &user.MockUsecase{}
			defer uUsecase.AssertExpectations(t)

			uUsecase.On("GetUser", tt.args.id).Return(tt.mock.user, tt.mock.err).Once()

			// setup http test
			urlAddr := fmt.Sprintf("/users/%d", tt.args.id)
			req, err := http.NewRequest(http.MethodGet, urlAddr, nil)
			require.NoError(t, err)
			wRec := httptest.NewRecorder()

			// test
			m := mux.NewRouter().PathPrefix("/users").Subrouter()
			NewUserHTTPHandler(m, func(p models.Role) user.Usecase { return uUsecase })
			m.ServeHTTP(wRec, req)

			body := wRec.Body.String()
			// check
			require.Equal(t, tt.want.statusCode, wRec.Code, body)
			require.NoError(t, err, "failed read body")
			require.JSONEq(t, tt.want.body, body)
		})
	}
}

func Test_userHandler_DeleteByID(t *testing.T) {
	type args struct {
		id int64
	}
	type mocks struct {
		err error
	}
	type want struct {
		statusCode int
	}
	tests := []struct {
		name string
		args args
		mock mocks
		want want
	}{
		{
			name: "Not exist",
			args: args{
				id: 42,
			},
			mock: mocks{
				err: models.ErrNotFound,
			},
			want: want{
				statusCode: http.StatusNotFound,
			},
		},
		{
			name: "Exist",
			args: args{
				id: 42,
			},
			mock: mocks{
				err: nil,
			},
			want: want{
				statusCode: http.StatusNoContent,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup mock
			uUsecase := &user.MockUsecase{}
			defer uUsecase.AssertExpectations(t)

			uUsecase.On("DeleteUser", tt.args.id).Return(tt.mock.err).Once()

			// setup http test
			urlAddr := fmt.Sprintf("/users/%d", tt.args.id)
			req, err := http.NewRequest(http.MethodDelete, urlAddr, nil)
			require.NoError(t, err)
			wRec := httptest.NewRecorder()

			// test
			m := mux.NewRouter().PathPrefix("/users").Subrouter()
			NewUserHTTPHandler(m, func(p models.Role) user.Usecase { return uUsecase })
			m.ServeHTTP(wRec, req)

			body := wRec.Body.String()
			// check
			require.Equal(t, tt.want.statusCode, wRec.Code, body)
		})
	}
}
