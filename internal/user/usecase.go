package user

import "bitbucket.org/ghostiam/userRESTAPI/models"

//go:generate gorunpkg github.com/vektra/mockery/cmd/mockery -inpkg -name Usecase
type Usecase interface {
	GetUsers() ([]User, error)
	StoreUser(name, password string, role models.Role) (*User, []error)
	GetUser(id int64) (*User, error)
	DeleteUser(id int64) error
}

type User struct {
	ID   int64
	Name string
	Role models.Role
}
