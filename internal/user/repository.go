package user

import "bitbucket.org/ghostiam/userRESTAPI/models"

//go:generate gorunpkg github.com/vektra/mockery/cmd/mockery -inpkg -name Repository
type Repository interface {
	Fetch(roles []models.Role) ([]models.User, error)
	GetByID(id int64) (*models.User, error)
	GetByName(name string) (*models.User, error)
	Store(u models.User) (int64, error)
	Delete(id int64) error
}
