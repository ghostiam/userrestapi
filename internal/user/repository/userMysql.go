package repository

import (
	"database/sql"

	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/jmoiron/sqlx"
)

type mysqlUserRepository struct {
	DB *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) user.Repository {
	return &mysqlUserRepository{
		DB: db,
	}
}

func (r *mysqlUserRepository) Fetch(roles []models.Role) ([]models.User, error) {
	userModels := make([]models.User, 0)

	if len(roles) == 0 {
		return userModels, nil
	}

	query := "SELECT * FROM users WHERE role IN (?)"
	query, args, err := sqlx.In(query, roles)
	if err != nil {
		return nil, err
	}

	err = r.DB.Select(&userModels, query, args...)
	return userModels, err
}

func (r *mysqlUserRepository) GetByID(id int64) (*models.User, error) {
	query := "SELECT * FROM users WHERE id = ? LIMIT 1"

	var userModel models.User
	err := r.DB.Get(&userModel, query, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return nil, err
	}

	return &userModel, nil
}

func (r *mysqlUserRepository) GetByName(name string) (*models.User, error) {
	query := "SELECT * FROM users WHERE name = ?"

	var userModel models.User
	err := r.DB.Get(&userModel, query, name)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return nil, err
	}

	return &userModel, nil
}

func (r *mysqlUserRepository) Store(u models.User) (int64, error) {
	query := "INSERT INTO users " +
		"(`name`,`role`,`encoded_password`,`salt`) " +
		"VALUES (?, ?, ?, ?);"

	res, err := r.DB.Exec(query,
		u.Name, u.Role, u.EncodedPassword, u.Salt)

	if err != nil {
		return 0, err
	}

	return res.LastInsertId()
}

func (r *mysqlUserRepository) Delete(id int64) error {
	query := "DELETE FROM users WHERE id = ?"
	_, err := r.DB.Exec(query, id)
	return err
}
