package usecase

import (
	"strings"

	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/pkg/errors"
)

type userUsecase struct {
	privileges models.Role
	repo       user.Repository
}

// TODO: написать доку получше
// New создаёт экземпляр объекта Usecase с ролью privileges для ограничения прав.
func New(privileges models.Role, ur user.Repository) user.Usecase {
	return &userUsecase{
		privileges: privileges,
		repo:       ur,
	}
}

func (u *userUsecase) GetUsers() ([]user.User, error) {
	var filter []models.Role

	if u.privileges == models.RoleAdmin {
		filter = []models.Role{models.RoleAdmin, models.RoleUser}
	} else if u.privileges == models.RoleUser {
		filter = []models.Role{models.RoleUser}
	}

	uModels, err := u.repo.Fetch(filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed fetch users")
	}

	// slice converter
	users := make([]user.User, 0)
	for _, uModel := range uModels {
		uUser := convertUser(&uModel)
		users = append(users, *uUser)
	}

	return users, nil
}

func (u *userUsecase) StoreUser(name, password string, role models.Role) (*user.User, []error) {
	errs := u.isStoreUserValid(name, password, role)
	if len(errs) > 0 {
		return nil, errs
	}

	userModel := models.User{
		Name: name,
		Role: role,
	}

	err := userModel.SetPassword(password)
	if err != nil {
		return nil, []error{errors.Wrap(err, "failed set password to user model")}
	}

	id, err := u.repo.Store(userModel)
	if err != nil {
		return nil, []error{errors.Wrap(err, "failed store user model")}
	}

	userModel.ID = id
	return convertUser(&userModel), nil
}

func (u *userUsecase) GetUser(id int64) (*user.User, error) {
	uUser, err := u.getUserByID(id)
	if err != nil {
		return nil, err
	}

	if !u.hasRoleAccess(uUser.Role) {
		return nil, models.ErrNotFound
	}

	return uUser, nil
}

func (u *userUsecase) DeleteUser(id int64) error {
	uUser, err := u.getUserByID(id)
	if err != nil {
		return err
	}

	if !u.hasRoleAccess(uUser.Role) {
		return models.ErrNotFound
	}

	err = u.repo.Delete(uUser.ID)
	return errors.Wrapf(err, "failed delete user by id %d", id)
}

// region Helpers
func (u *userUsecase) getUserByID(id int64) (*user.User, error) {
	userModel, err := u.repo.GetByID(id)
	if err != nil {
		return nil, errors.Wrapf(err, "failed get user by id %d", id)
	}

	if userModel == nil {
		return nil, models.ErrNotFound
	}

	return convertUser(userModel), nil
}

func convertUser(um *models.User) *user.User {
	if um == nil {
		return nil
	}

	return &user.User{
		ID:   um.ID,
		Name: um.Name,
		Role: um.Role,
	}
}

func (u *userUsecase) isStoreUserValid(name, password string, role models.Role) []error {
	var errs []error

	isEmptyName := strings.TrimSpace(name) == ""
	if isEmptyName {
		errs = append(errs, models.ErrUserNameEmpty)
	} else {
		isExist, err := u.isExistUserByName(name)
		if err != nil {
			return []error{errors.Wrap(err, "failed check user name exist")}
		}

		if isExist {
			errs = append(errs, models.ErrUserNameConflict)
		}
	}

	isEmptyPassword := strings.TrimSpace(password) == ""
	if isEmptyPassword {
		errs = append(errs, models.ErrUserPasswordEmpty)
	}

	if !role.IsValid() {
		errs = append(errs, models.ErrUserRoleInvalid)
	}

	if !u.hasRoleAccess(role) {
		errs = append(errs, models.ErrUserRoleRejected)
	}

	return errs
}

func (u *userUsecase) isExistUserByName(name string) (bool, error) {
	userModel, err := u.repo.GetByName(name)
	if err != nil {
		return false, errors.Wrapf(err, "failed get user by name (%s)", name)
	}

	if userModel == nil {
		return false, nil
	}

	return true, nil
}

func (u *userUsecase) hasRoleAccess(role models.Role) bool {
	if u.privileges == models.RoleAdmin {
		return true
	}

	if u.privileges == models.RoleUser && role == models.RoleUser {
		return true
	}

	return false
}

// endregion
