package usecase

import (
	"testing"

	"bitbucket.org/ghostiam/userRESTAPI/internal/user"
	"bitbucket.org/ghostiam/userRESTAPI/models"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func Test_userUsecase_GetUsers(t *testing.T) {
	type fields struct {
		privileges models.Role
	}
	type mocks struct {
		users []models.User
		err   error
	}
	type want struct {
		roles []models.Role
		users []user.User
		err   error
	}
	tests := []struct {
		name   string
		fields fields
		mock   mocks
		want   want
	}{
		{
			name: "Empty list",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			mock: mocks{
				users: nil,
				err:   nil,
			},
			want: want{
				roles: []models.Role{"admin", "user"},
				users: []user.User{},
				err:   nil,
			},
		},
		{
			name: "Users list",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			mock: mocks{
				users: []models.User{
					{ID: 123, Role: models.RoleAdmin},
					{ID: 456, Role: models.RoleUser},
				},
				err: nil,
			},
			want: want{
				roles: []models.Role{"admin", "user"},
				users: []user.User{
					{ID: 123, Role: models.RoleAdmin},
					{ID: 456, Role: models.RoleUser},
				},
				err: nil,
			},
		},
		{
			name: "Admin get list with admin and user roles",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			want: want{
				roles: []models.Role{"admin", "user"},
				users: []user.User{},
			},
		},
		{
			name: "User get list with only user role",
			fields: fields{
				privileges: models.RoleUser,
			},
			want: want{
				roles: []models.Role{"user"},
				users: []user.User{},
			},
		},
		{
			name: "Unknown role get list with empty roles",
			fields: fields{
				privileges: models.RoleUnknown,
			},
			want: want{
				roles: nil,
				users: []user.User{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup
			repo := &user.MockRepository{}
			defer repo.AssertExpectations(t)

			repo.On("Fetch", tt.want.roles).Return(tt.mock.users, tt.mock.err).Once()

			// test
			u := New(tt.fields.privileges, repo)
			gotUsers, err := u.GetUsers()

			// check
			assert.Equal(t, tt.want.err, err)
			assert.Equal(t, tt.want.users, gotUsers)
		})
	}
}

func Test_userUsecase_StoreUser(t *testing.T) {
	type fields struct {
		privileges models.Role
	}
	type args struct {
		name     string
		password string
		role     models.Role
	}
	type mocks struct {
		get struct {
			user *models.User
			err  error
		}
		store struct {
			id  int64
			err error
		}
	}
	type want struct {
		user *user.User
		errs []error
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		mock   mocks
		want   want
	}{
		// End check validating
		{
			name: "Empty all validate",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "",
				password: "",
				role:     "",
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserNameEmpty, models.ErrUserPasswordEmpty, models.ErrUserRoleInvalid},
			},
		},
		{
			name: "Empty name validate",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "",
				password: "TestPass",
				role:     models.RoleUser,
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserNameEmpty},
			},
		},
		{
			name: "Empty password validate",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "TestName",
				password: "",
				role:     models.RoleUser,
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserPasswordEmpty},
			},
		},
		{
			name: "Empty role validate",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "TestName",
				password: "TestPass",
				role:     "",
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserRoleInvalid},
			},
		},
		{
			name: "Invalid role validate",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "TestName",
				password: "TestPass",
				role:     "Invalid",
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserRoleInvalid},
			},
		},
		{
			name: "Conflict name validate",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "Conflict",
				password: "TestPass",
				role:     models.RoleUser,
			},
			mock: mocks{
				get: struct {
					user *models.User
					err  error
				}{
					user: &models.User{
						ID:   42,
						Name: "Conflict",
						Role: models.RoleUser,
					},
					err: nil,
				},
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserNameConflict},
			},
		},
		{
			name: "User can't store Admin role validate",
			fields: fields{
				privileges: models.RoleUser,
			},
			args: args{
				name:     "TestName",
				password: "TestPass",
				role:     models.RoleAdmin,
			},
			want: want{
				user: nil,
				errs: []error{models.ErrUserRoleRejected},
			},
		},
		// End check validating
		// //////////////////////////////////////////////////////////
		// Start check mocking
		{
			name: "Admin store Admin",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "TestName",
				password: "TestPass",
				role:     models.RoleAdmin,
			},
			mock: mocks{
				store: struct {
					id  int64
					err error
				}{
					id:  42,
					err: nil,
				},
			},
			want: want{
				user: &user.User{
					ID:   42,
					Name: "TestName",
					Role: models.RoleAdmin,
				},
				errs: nil,
			},
		},
		{
			name: "Admin store User",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				name:     "TestName",
				password: "TestPass",
				role:     models.RoleUser,
			},
			mock: mocks{
				store: struct {
					id  int64
					err error
				}{
					id:  42,
					err: nil,
				},
			},
			want: want{
				user: &user.User{
					ID:   42,
					Name: "TestName",
					Role: models.RoleUser,
				},
				errs: nil,
			},
		},
		{
			name: "User store User",
			fields: fields{
				privileges: models.RoleUser,
			},
			args: args{
				name:     "TestName",
				password: "TestPass",
				role:     models.RoleUser,
			},
			mock: mocks{
				store: struct {
					id  int64
					err error
				}{
					id:  42,
					err: nil,
				},
			},
			want: want{
				user: &user.User{
					ID:   42,
					Name: "TestName",
					Role: models.RoleUser,
				},
				errs: nil,
			},
		},
		// End check mocking
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup
			repo := &user.MockRepository{}
			defer repo.AssertExpectations(t)

			// for check name conflict
			if tt.args.name != "" {
				repo.On("GetByName", tt.args.name).Return(tt.mock.get.user, tt.mock.get.err).Once()
			}
			if tt.mock.store.id > 0 {
				repo.On("Store", mock.Anything).Return(tt.mock.store.id, tt.mock.store.err).Once()
			}

			// test
			u := New(tt.fields.privileges, repo)
			gotUser, errs := u.StoreUser(tt.args.name, tt.args.password, tt.args.role)

			// check
			assert.Equal(t, tt.want.errs, errs)
			assert.Equal(t, tt.want.user, gotUser)
		})
	}
}

func Test_userUsecase_GetUser(t *testing.T) {
	type fields struct {
		privileges models.Role
	}
	type args struct {
		id int64
	}
	type mocks struct {
		user *models.User
		err  error
	}
	type want struct {
		user *user.User
		err  error
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		mock   mocks
		want   want
	}{
		{
			name: "Not exist user",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				user: nil,
				err:  nil,
			},
			want: want{
				user: nil,
				err:  models.ErrNotFound,
			},
		},
		{
			name: "Admin get Admin",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				user: &models.User{
					ID:   42,
					Role: models.RoleAdmin,
				},
				err: nil,
			},
			want: want{
				user: &user.User{
					ID:   42,
					Role: models.RoleAdmin,
				},
				err: nil,
			},
		},
		{
			name: "Admin get User",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				user: &models.User{
					ID:   42,
					Role: models.RoleUser,
				},
				err: nil,
			},
			want: want{
				user: &user.User{
					ID:   42,
					Role: models.RoleUser,
				},
				err: nil,
			},
		},
		{
			name: "User can't get Admin",
			fields: fields{
				privileges: models.RoleUser,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				user: &models.User{
					ID:   42,
					Role: models.RoleAdmin,
				},
				err: nil,
			},
			want: want{
				user: nil,
				err:  models.ErrNotFound,
			},
		},
		{
			name: "User get User",
			fields: fields{
				privileges: models.RoleUser,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				user: &models.User{
					ID:   42,
					Name: "Test User",
					Role: models.RoleUser,
				},
				err: nil,
			},
			want: want{
				user: &user.User{
					ID:   42,
					Name: "Test User",
					Role: models.RoleUser,
				},
				err: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup
			repo := &user.MockRepository{}
			defer repo.AssertExpectations(t)

			repo.On("GetByID", tt.args.id).Return(tt.mock.user, tt.mock.err).Once()

			// test
			u := New(tt.fields.privileges, repo)
			gotUser, err := u.GetUser(tt.args.id)

			// check
			assert.Equal(t, tt.want.err, err)
			assert.Equal(t, tt.want.user, gotUser)
		})
	}
}

func Test_userUsecase_DeleteUser(t *testing.T) {
	type fields struct {
		privileges models.Role
	}
	type args struct {
		id int64
	}
	type mocks struct {
		getUser   *models.User
		getErr    error
		deleteErr error
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    mocks
		wantErr error
	}{
		{
			name: "Not exist",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				getUser:   nil,
				getErr:    nil,
				deleteErr: nil,
			},
			wantErr: models.ErrNotFound,
		},
		{
			name: "Admin delete Admin",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				getUser: &models.User{
					ID:   42,
					Role: models.RoleAdmin,
				},
				getErr:    nil,
				deleteErr: nil,
			},
			wantErr: nil,
		},
		{
			name: "Admin delete User",
			fields: fields{
				privileges: models.RoleAdmin,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				getUser: &models.User{
					ID:   42,
					Role: models.RoleUser,
				},
				getErr:    nil,
				deleteErr: nil,
			},
			wantErr: nil,
		},
		{
			name: "User can't delete Admin",
			fields: fields{
				privileges: models.RoleUser,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				getUser: &models.User{
					ID:   42,
					Role: models.RoleAdmin,
				},
				getErr:    nil,
				deleteErr: nil,
			},
			wantErr: models.ErrNotFound,
		},
		{
			name: "User delete User",
			fields: fields{
				privileges: models.RoleUser,
			},
			args: args{
				id: 42,
			},
			mock: mocks{
				getUser: &models.User{
					ID:   42,
					Role: models.RoleUser,
				},
				getErr:    nil,
				deleteErr: nil,
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup
			repo := &user.MockRepository{}
			defer repo.AssertExpectations(t)

			repo.On("GetByID", tt.args.id).Return(tt.mock.getUser, tt.mock.getErr).Once()
			repo.On("Delete", tt.args.id).Return(tt.mock.deleteErr).Maybe()

			// test
			u := New(tt.fields.privileges, repo)
			err := u.DeleteUser(tt.args.id)

			// check
			assert.Equal(t, tt.wantErr, err)
		})
	}
}
