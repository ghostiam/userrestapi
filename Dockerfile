# build stage
FROM golang:alpine AS builder

ARG MODULE_PATH=bitbucket.org/ghostiam/userRESTAPI

RUN apk add --update make git gcc musl-dev
ADD . /go/src/$MODULE_PATH
RUN cd /go/src/$MODULE_PATH && make deps && make test && make build
RUN cp -R /go/src/$MODULE_PATH/dist /app

# final stage
FROM scratch
WORKDIR /
COPY --from=builder /app /
CMD ["/server"]